#pragma once
#include <QLabel>
#include <QPixmap>
#include <map>
#include <string_view>
using str_t = std::string_view;
using direction_map_t = std::map<str_t, QPixmap>; 
class gamepad_monitor;


namespace ui {
class stick_label : public QLabel {
    Q_OBJECT

public:
    stick_label(gamepad_monitor* gm, QWidget* parent=nullptr);
public Q_SLOT:
    void set_pixmap(char id, double direction);

private:
    char _id;
    direction_map_t _direction;
};
} // namespace ui
