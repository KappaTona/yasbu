#pragma once
#include "image_label.h"
#include "gamepad_monitor.h"
#include <queue>
#include <string_view>
#include <QWidget>
#include <QGridLayout>
#include <QPointer>
using QGridLayoutPtr = QPointer<QGridLayout>;
using ordered_images_t = std::queue<default_selected_pair_t>;
class QDialog;


namespace ui {
class tracker: public QWidget {
    Q_OBJECT

public:
    tracker(QDialog* diag);
public Q_SLOT:
    void select_btn(char id);
private:
    void create_three_x_three();
    image_label* make_action_button();

    QGridLayoutPtr _tracker = new QGridLayout;
    gamepad_monitor _gamepad_monitor;
    ordered_images_t _action_buttons;
};
}// namespace ui
