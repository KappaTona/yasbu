cmake_minimum_required(VERSION 3.10)


add_executable(tracker 
    main.cpp 
    tracker.cpp
    setup.cpp
    image_label.cpp
    stick_label.cpp
    ${CMAKE_SOURCE_DIR}/res/buttons.qrc
)
target_link_libraries(tracker 
    Qt5::Core 
    Qt5::Widgets
    gamepad_monitor
)
