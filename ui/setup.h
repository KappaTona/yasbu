#ifndef setup_header
#define setup_header

#include <QObject>
#include <QDialog>
#include <QPointer>

class setup : public QObject
{
	Q_OBJECT
	QPointer<QDialog> view_; 
public:
	setup(int w, int h);
	QDialog* main_view() const;
public Q_SLOTS:
	void clean_up();
};

#endif
