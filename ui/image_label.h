#pragma once
#include <QLabel>
#include <QPixmap>
#include <map>
#include <utility>
#include <string_view>
using str_t = std::string_view;
using image_resources_t = std::map<str_t, QPixmap>;
using default_selected_pair_t = std::pair<str_t, str_t>;
class gamepad_monitor;


namespace ui {
class image_label : public QLabel {
    Q_OBJECT

public:
    image_label(default_selected_pair_t const& img_res, gamepad_monitor* gm,
            QWidget* parent=nullptr);

public Q_SLOT:
    void set_pixmap(char id, bool pressed);

private:
    image_resources_t _img_res;
    char _id;
    bool _pressed = false;
};
} //namespace ui
