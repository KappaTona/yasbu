#include "tracker.h"
#include "stick_label.h"
#include <iostream>
#include <vector>
#include <utility>
#include <memory>
#include <QScrollArea>
#include <QPalette>
#include <QDialog>
#include <QResource>
using QScrollAreaPtr = QPointer<QScrollArea>;


namespace detail {

ordered_images_t make_oredered_images(
        std::vector<str_t>&& defs, std::vector<str_t>&& sels)
{
    ordered_images_t rv;
    for (int i = 0; i < defs.size() && i < sels.size(); ++i)
        rv.emplace(std::make_pair(defs[i], sels[i]));
    return rv;
}

} //namespace

namespace ui {
tracker::tracker(QDialog* diag)
{
    QResource::registerResource("res/buttons.qrc");
    _action_buttons = detail::make_oredered_images(
            {":/y", ":/x", ":/b", ":/a"},
            {":/y_s", ":/x_s", ":/b_s", ":/a_s"});

    create_three_x_three();
    diag->setLayout(_tracker);
}

void tracker::create_three_x_three()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            QScrollAreaPtr current = new QScrollArea;
            current->setBackgroundRole(QPalette::Dark);
            current->setAlignment(Qt::AlignCenter);

            if ((i + j) % 2) {
                current->setWidget(make_action_button());
            }
            if (i == 1 && j == 1) {
                current->setWidget(new stick_label{&_gamepad_monitor});
            }
            _tracker->addWidget(current, i, j);
        }
    }
}

image_label* tracker::make_action_button()
{
    auto image_label_ptr = std::make_unique<image_label>(
        _action_buttons.front(), &_gamepad_monitor);

    _action_buttons.pop();
    return image_label_ptr.release();
}
}// namespace ui
