#include "setup.h"
#include "tracker.h"
#include <QApplication>
#include <QDialog>
#include <QPointer>
using ui::tracker;

int main(int argc, char** argv)
{
	int return_from_event_code = 0;
	int reset_code = 1000;
	QPointer<QApplication> app;
	QPointer<setup> s;
	QPointer<tracker> mb;
	do
	{
		if (app) delete app;
		if (s) delete s;
		if (mb) delete mb;

		app = new QApplication{argc, argv};
		s = new setup{1024, 769};
		mb = new tracker{s->main_view()};
		return_from_event_code = app->exec();
	}
	while (return_from_event_code == reset_code);
	return return_from_event_code;
}
