#include "image_label.h"
#include "gamepad_monitor.h"
#include <QImage>


namespace ui {
image_label::image_label(default_selected_pair_t const& img_res,
        gamepad_monitor* gm, QWidget* parent)
    : QLabel{parent}
{
    _img_res["default"] = QPixmap::fromImage(QImage{img_res.first.data()});
    _img_res["selected"] = QPixmap::fromImage(QImage{img_res.second.data()});
    _id = img_res.first.data()[2];
    
    connect(gm, &gamepad_monitor::pressed, this, &image_label::set_pixmap);
    setPixmap(_img_res.at("default"));
}

void image_label::set_pixmap(char id, bool pressed)
{
    if (_id != id) {
        return;
    }
    _pressed = pressed;

    if (_pressed) {
        setPixmap(_img_res.at("selected"));
    }
    else {
        setPixmap(_img_res.at("default"));
    }
}
} // namespace ui
