#include "stick_label.h"
#include "gamepad_monitor.h"


namespace detail {
QPixmap make_qpixmap(str_t source)
{
    return QPixmap::fromImage(QImage{source.data()});
}
}

namespace ui {
stick_label::stick_label(gamepad_monitor* gm, QWidget* parent) : QLabel{parent}
{
    _direction["default"] = detail::make_qpixmap(":/s");
    _direction["up"] = detail::make_qpixmap(":/su");
    _direction["left"] = detail::make_qpixmap(":/sl");
    _direction["right"] = detail::make_qpixmap(":/sr");
    _direction["down"] = detail::make_qpixmap(":/sd");

    connect(gm, &gamepad_monitor::changed, this, &stick_label::set_pixmap);
    setPixmap(_direction.at("default"));
}

void stick_label::set_pixmap(char id, double direction)
{
    if ((id == 'h' || id == 'v') && direction == 0) {
        setPixmap(_direction["default"]);
        return;
    }

    if (id == 'h') {
        if (direction > 0) {
            setPixmap(_direction["right"]);
        }
        if (direction < 0) {
            setPixmap(_direction["left"]);
        }
    }

    if (id == 'v') {
        if (direction > 0) {
            setPixmap(_direction["up"]);
        }
        if (direction < 0) {
            setPixmap(_direction["down"]);
        }
    }
}
} //namespace ui
