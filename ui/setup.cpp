#include "setup.h"
#include <QCoreApplication>
#include <QDebug>


setup::setup(int w, int h)
{
	view_ = new QDialog;
	view_->setFixedWidth(w);
	view_->setFixedHeight(h);
	view_->show();
}

void setup::clean_up()
{
	delete view_;
}

QDialog* setup::main_view() const
{
	return view_;
}
