#ifndef GAMEPADMONITOR_H
#define GAMEPADMONITOR_H
#include <QObject>
#include <QGamepad>
#include <memory>
class QGamepad;


class gamepad_monitor : public QObject
{
    Q_OBJECT

public:
    explicit gamepad_monitor(QObject *parent=nullptr);

Q_SIGNALS:
    void pressed(char id, bool pressed);
    void changed(char id, double value);

private:
    std::unique_ptr<QGamepad> _gamepad;
    void create_connections();
    void emit_pressed(char id, bool pressed);
    void emit_changed(char id, double value);
};

#endif // GAMEPADMONITOR_H
