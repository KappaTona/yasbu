#include "gamepad_monitor.h"


gamepad_monitor::gamepad_monitor(QObject *parent)
    : QObject{parent}
{
    auto gamepads = QGamepadManager::instance()->connectedGamepads();
    if (gamepads.isEmpty()) {
        throw "Did not find any connected gamepads";
    }
    _gamepad = std::make_unique<QGamepad>(*gamepads.begin(), this);

    create_connections();
}

void gamepad_monitor::create_connections()
{
    connect(_gamepad.get(), &QGamepad::buttonAChanged, [this](bool pressed){
            emit_pressed('a', pressed);
    });
    connect(_gamepad.get(), &QGamepad::buttonYChanged, [this](bool pressed){
            emit_pressed('x', pressed);
    });
    connect(_gamepad.get(), &QGamepad::buttonXChanged, [this](bool pressed){
            emit_pressed('y', pressed);
    });
    connect(_gamepad.get(), &QGamepad::buttonBChanged, [this](bool pressed){
            emit_pressed('b', pressed);
    });

//   TODO char as id sucks dick...
    connect(_gamepad.get(), &QGamepad::axisLeftXChanged, [this](double value){
            emit_changed('h', value);
    });
    connect(_gamepad.get(), &QGamepad::axisLeftYChanged, [this](double value){
            emit_changed('v', value);
    });
}

void gamepad_monitor::emit_pressed(char id, bool is_pressed)
{
    Q_EMIT pressed(id, is_pressed);
}

void gamepad_monitor::emit_changed(char id, double value)
{
    Q_EMIT changed(id, value);
}
